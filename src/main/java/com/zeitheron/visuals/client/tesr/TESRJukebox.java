package com.zeitheron.visuals.client.tesr;

import com.zeitheron.hammercore.client.render.tesr.TESR;
import com.zeitheron.visuals.tiles.TileEntityJukeboxFixed;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms.TransformType;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

public class TESRJukebox extends TESR<TileEntityJukeboxFixed>
{
	@Override
	public void renderTileEntityAt(TileEntityJukeboxFixed te, double x, double y, double z, float partialTicks, ResourceLocation destroyStage, float alpha)
	{
		ItemStack record = te.getRecord();
		if(record.isEmpty() && !te.dropStack.isEmpty())
			record = te.dropStack;
		
		float prog = (float) Math.sin(Math.toRadians(90 * (te.prevInsertProgress + (te.insertProgress - te.prevInsertProgress) * partialTicks)));
		
		prog *= .15F;
		
		GlStateManager.pushMatrix();
		GlStateManager.translate(x + .5, y + 1 - prog, z + .5);
		GlStateManager.rotate(90, 0, 1, 0);
		
		mc.getRenderItem().renderItem(record, TransformType.GROUND);
		
		GlStateManager.popMatrix();
	}
}