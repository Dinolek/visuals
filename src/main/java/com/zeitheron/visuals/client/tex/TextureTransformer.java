package com.zeitheron.visuals.client.tex;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.zeitheron.hammercore.client.utils.IImagePreprocessor;
import com.zeitheron.hammercore.utils.color.ColorHelper;
import com.zeitheron.visuals.client.GLDownloader;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.texture.AbstractTexture;
import net.minecraft.client.renderer.texture.DynamicTexture;
import net.minecraft.client.renderer.texture.ITextureObject;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.renderer.texture.TextureUtil;
import net.minecraft.client.resources.IResource;
import net.minecraft.client.resources.IResourceManager;
import net.minecraft.client.resources.data.TextureMetadataSection;
import net.minecraft.init.Blocks;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class TextureTransformer
{
	public static final Map<ResourceLocation, IImagePreprocessor> processors = new HashMap<>();
	public static final List<ResourceLocation> textures = new ArrayList<>();
	
	public static final IImagePreprocessor CHEST_SINGLE_SAW = image ->
	{
		boolean cut;
		float px, py;
		
		BufferedImage i = new BufferedImage(image.getWidth(), image.getHeight(), image.getType());
		for(int x = 0; x < i.getWidth(); ++x)
			for(int y = 0; y < i.getHeight(); ++y)
			{
				int rgb = image.getRGB(x, y);
				
				px = x / (float) i.getWidth();
				py = y / (float) i.getHeight();
				cut = (px >= 15 / 64F && py >= 20 / 64F && px < 27 / 64F && py < 32 / 64F && ((rgb >> 16) & 0xFF) + ((rgb >> 8) & 0xFF) + (rgb & 0xFF) < 10);
				
				if(!cut)
					i.setRGB(x, y, rgb);
			}
		
		return i;
	};
	
	public static final IImagePreprocessor CHEST_DOUBLE_SAW = image ->
	{
		boolean cut;
		float px, py;
		
		BufferedImage i = new BufferedImage(image.getWidth(), image.getHeight(), image.getType());
		for(int x = 0; x < i.getWidth(); ++x)
			for(int y = 0; y < i.getHeight(); ++y)
			{
				int rgb = image.getRGB(x, y);
				
				px = x / (float) i.getWidth();
				py = y / (float) i.getHeight();
				cut = (px >= 15 / 128F && py >= 20 / 64F && px < 43 / 128F && py < 32 / 64F && ((rgb >> 16) & 0xFF) + ((rgb >> 8) & 0xFF) + (rgb & 0xFF) < 10);
				
				if(!cut)
					i.setRGB(x, y, rgb);
			}
		
		return i;
	};
	
	public static final IImagePreprocessor CHEST_ENDER_SAW = image ->
	{
		boolean cut;
		float px, py;
		
		BufferedImage i = new BufferedImage(image.getWidth(), image.getHeight(), image.getType());
		for(int x = 0; x < i.getWidth(); ++x)
			for(int y = 0; y < i.getHeight(); ++y)
			{
				int rgb = image.getRGB(x, y);
				
				px = x / (float) i.getWidth();
				py = y / (float) i.getHeight();
				cut = (px >= 16 / 64F && py >= 21 / 64F && px < 26 / 64F && py < 31 / 64F);
				
				if(!cut)
					i.setRGB(x, y, rgb);
			}
		
		return i;
	};
	
	public static final IImagePreprocessor CHEST_ENDER_INSIDE_TRANSPARENT = image ->
	{
		ResourceLocation nr = new ResourceLocation("minecraft", "textures/entity/chest/ender.png");
		Minecraft.getMinecraft().getTextureManager().bindTexture(nr);
		image = GLDownloader.toBufferedImage(Minecraft.getMinecraft().getTextureManager().getTexture(nr).getGlTextureId());
		
		boolean cut;
		float px, py;
		
		BufferedImage i = new BufferedImage(image.getWidth(), image.getHeight(), image.getType());
		
		for(int x = 0; x < i.getWidth(); ++x)
			for(int y = 0; y < i.getHeight(); ++y)
			{
				int rgb = image.getRGB(x, y);
				
				px = x / (float) i.getWidth();
				py = y / (float) i.getHeight();
				cut = (px >= 30 / 64F && py >= 2 / 64F && px < 40 / 64F && py < 12 / 64F);
				
				if(!cut)
					i.setRGB(x, y, rgb);
				else
				{
					float r = ColorHelper.getRed(rgb);
					float g = ColorHelper.getGreen(rgb);
					float b = ColorHelper.getBlue(rgb);
					float a = Math.max(0, Math.min(1, (1F - (py - 2 / 64F) / (10 / 64F))));
					
					i.setRGB(x, y, ColorHelper.packARGB(a, r, g, b));
				}
			}
		
		return i;
	};
	
	public static int lampsWidth;
	
	static
	{
		transform(new ResourceLocation("minecraft", "textures/entity/chest/christmas.png"), CHEST_SINGLE_SAW);
		transform(new ResourceLocation("minecraft", "textures/entity/chest/normal.png"), CHEST_SINGLE_SAW);
		transform(new ResourceLocation("minecraft", "textures/entity/chest/trapped.png"), CHEST_SINGLE_SAW);
		transform(new ResourceLocation("minecraft", "textures/entity/chest/christmas_double.png"), CHEST_DOUBLE_SAW);
		transform(new ResourceLocation("minecraft", "textures/entity/chest/normal_double.png"), CHEST_DOUBLE_SAW);
		transform(new ResourceLocation("minecraft", "textures/entity/chest/trapped_double.png"), CHEST_DOUBLE_SAW);
		transform(new ResourceLocation("minecraft", "textures/entity/chest/ender.png"), CHEST_ENDER_SAW);
		transform(new ResourceLocation("visuals", "textures/entity/chest/ender.png"), CHEST_ENDER_INSIDE_TRANSPARENT);
		
		transform(new ResourceLocation("ironchest", "textures/model/chest/copper_chest.png"), CHEST_SINGLE_SAW);
		transform(new ResourceLocation("ironchest", "textures/model/chest/diamond_chest.png"), CHEST_SINGLE_SAW);
		transform(new ResourceLocation("ironchest", "textures/model/chest/dirt_chest.png"), CHEST_SINGLE_SAW);
		transform(new ResourceLocation("ironchest", "textures/model/chest/gold_chest.png"), CHEST_SINGLE_SAW);
		transform(new ResourceLocation("ironchest", "textures/model/chest/iron_chest.png"), CHEST_SINGLE_SAW);
		transform(new ResourceLocation("ironchest", "textures/model/chest/obsidian_chest.png"), CHEST_SINGLE_SAW);
		transform(new ResourceLocation("ironchest", "textures/model/chest/silver_chest.png"), CHEST_SINGLE_SAW);
		
		transform(new ResourceLocation("visuals", "textures/builtin/lamp.png"), nul ->
		{
			BufferedImage atlas = GLDownloader.toBufferedImage(Minecraft.getMinecraft().getTextureManager().getTexture(TextureMap.LOCATION_BLOCKS_TEXTURE).getGlTextureId());
			TextureAtlasSprite onTex = Minecraft.getMinecraft().getBlockRendererDispatcher().getModelForState(Blocks.LIT_REDSTONE_LAMP.getDefaultState()).getParticleTexture();
			TextureAtlasSprite offTex = Minecraft.getMinecraft().getBlockRendererDispatcher().getModelForState(Blocks.REDSTONE_LAMP.getDefaultState()).getParticleTexture();
			BufferedImage off = GLDownloader.exportSprite(offTex, atlas);
			BufferedImage on = GLDownloader.exportSprite(onTex, atlas);
			int msw = lampsWidth = Math.min(on.getWidth(), off.getWidth());
			{
				BufferedImage temp = new BufferedImage(msw, msw, BufferedImage.TYPE_INT_ARGB);
				Graphics2D g = temp.createGraphics();
				g.drawImage(off, 0, 0, msw, msw, null);
				g.dispose();
				off = temp;
				
				temp = new BufferedImage(msw, msw, BufferedImage.TYPE_INT_ARGB);
				g = temp.createGraphics();
				g.drawImage(on, 0, 0, msw, msw, null);
				g.dispose();
				on = temp;
			}
			BufferedImage nat = new BufferedImage(msw * 10, msw * 10, BufferedImage.TYPE_INT_ARGB);
			Graphics2D g = nat.createGraphics();
			for(int atx = 0; atx < 10; ++atx)
				for(int aty = 0; aty < 10; ++aty)
				{
					int cur = atx + aty * 10;
					float progress = cur / 100F;
					
					BufferedImage res = new BufferedImage(msw, msw, BufferedImage.TYPE_INT_ARGB);
					
					for(int x = 0; x < msw; ++x)
						for(int y = 0; y < msw; ++y)
							res.setRGB(x, y, ColorHelper.interpolate(off.getRGB(x, y), on.getRGB(x, y), progress));
						
					g.drawImage(res, atx * msw, aty * msw, null);
				}
			g.dispose();
			return nat;
		});
	}
	
	public static void update()
	{
		TextureManager mgr = Minecraft.getMinecraft().getTextureManager();
		for(ResourceLocation texture : textures)
		{
			ITextureObject tex = mgr.getTexture(texture);
			if(tex != null && !(tex instanceof IFixedTex))
			{
				if(tex instanceof DynamicTexture && tex.getGlTextureId() == 1)
					mgr.loadTexture(texture, new FixedCachedTexture(texture, processors.get(texture), null));
				else
					try
					{
						BufferedImage before = GLDownloader.toBufferedImage(tex.getGlTextureId());
						GlStateManager.deleteTexture(tex.getGlTextureId());
						mgr.loadTexture(texture, new FixedCachedTexture(texture, processors.get(texture), before));
					} catch(IllegalArgumentException e)
					{
						mgr.loadTexture(texture, new FixedCachedTexture(texture, processors.get(texture), null));
					}
			}
		}
	}
	
	public static void transform(ResourceLocation tex, IImagePreprocessor proc)
	{
		if(!textures.contains(tex))
			textures.add(tex);
		
		if(processors.containsKey(tex))
			processors.put(tex, i -> processors.get(tex).process(proc.process(i)));
		else
			processors.put(tex, proc);
	}
	
	public static void bind(ResourceLocation texture)
	{
		TextureManager mgr = Minecraft.getMinecraft().getTextureManager();
		ITextureObject tex = mgr.getTexture(texture);
		if(!(tex instanceof IFixedTex))
		{
			if(tex != null)
			{
				BufferedImage before = GLDownloader.toBufferedImage(tex.getGlTextureId());
				GlStateManager.deleteTexture(tex.getGlTextureId());
				mgr.loadTexture(texture, new FixedCachedTexture(texture, processors.get(texture), before));
			} else
				mgr.loadTexture(texture, new FixedTexture(texture, processors.get(texture)));
		}
		mgr.bindTexture(texture);
	}
	
	public static interface IFixedTex
	{
	}
	
	public static class FixedCachedTexture extends AbstractTexture implements IFixedTex
	{
		private static final Logger LOGGER = LogManager.getLogger();
		
		final ResourceLocation texture;
		final IImagePreprocessor proc;
		final BufferedImage image;
		
		public FixedCachedTexture(ResourceLocation texture, IImagePreprocessor proc, BufferedImage image)
		{
			if(image == null)
			{
				image = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
				try
				{
					image.getRGB(0, 0, 16, 16, TextureUtil.MISSING_TEXTURE_DATA, 0, 16);
				} catch(Throwable err)
				{
					err.printStackTrace();
				}
			}
			
			this.image = image;
			this.texture = texture;
			this.proc = proc;
		}
		
		@Override
		public void loadTexture(IResourceManager resourceManager) throws IOException
		{
			this.deleteGlTexture();
			
			try
			{
				boolean flag = false;
				boolean flag1 = false;
				
				BufferedImage bufferedimage = image;
				
				if(proc != null)
					bufferedimage = proc.process(bufferedimage);
				
				TextureUtil.uploadTextureImageAllocate(this.getGlTextureId(), bufferedimage, flag, flag1);
			} finally
			{
				
			}
		}
	}
	
	public static class FixedTexture extends AbstractTexture implements IFixedTex
	{
		private static final Logger LOGGER = LogManager.getLogger();
		
		final ResourceLocation texture;
		final IImagePreprocessor proc;
		
		public FixedTexture(ResourceLocation texture, IImagePreprocessor proc)
		{
			this.texture = texture;
			this.proc = proc;
		}
		
		@Override
		public void loadTexture(IResourceManager resourceManager) throws IOException
		{
			this.deleteGlTexture();
			IResource iresource = null;
			
			try
			{
				iresource = resourceManager.getResource(this.texture);
				BufferedImage bufferedimage = TextureUtil.readBufferedImage(iresource.getInputStream());
				boolean flag = false;
				boolean flag1 = false;
				
				if(proc != null)
					bufferedimage = proc.process(bufferedimage);
				
				if(iresource.hasMetadata())
				{
					try
					{
						TextureMetadataSection texturemetadatasection = (TextureMetadataSection) iresource.getMetadata("texture");
						
						if(texturemetadatasection != null)
						{
							flag = texturemetadatasection.getTextureBlur();
							flag1 = texturemetadatasection.getTextureClamp();
						}
					} catch(RuntimeException runtimeexception)
					{
						LOGGER.warn("Failed reading metadata of: {}", this.texture, runtimeexception);
					}
				}
				
				TextureUtil.uploadTextureImageAllocate(this.getGlTextureId(), bufferedimage, flag, flag1);
			} finally
			{
				IOUtils.closeQuietly((Closeable) iresource);
			}
		}
	}
}