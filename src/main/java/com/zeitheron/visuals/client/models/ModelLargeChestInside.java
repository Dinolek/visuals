package com.zeitheron.visuals.client.models;

import com.zeitheron.hammercore.client.model.ModelSimple;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

public class ModelLargeChestInside extends ModelSimple<ResourceLocation>
{
	public ModelRenderer shape4;
	public ModelRenderer shape4_1;
	public ModelRenderer shape4_2;
	public ModelRenderer shape7;
	public ModelRenderer shape7_1;
	
	public ModelLargeChestInside()
	{
		super(128, 64, "");
		this.shape4_1 = new ModelRenderer(this, 15, 0);
		this.shape4_1.setRotationPoint(30.0F, 0.0F, 14.0F);
		this.shape4_1.addBox(0.0F, 6.0F, 0.0F, 28, 8, 1, 0.0F);
		this.setRotateAngle(shape4_1, 0.0F, 3.141592653589793F, 0.0F);
		this.shape4 = new ModelRenderer(this, 15, 0);
		this.shape4.setRotationPoint(2.0F, 6.0F, 2.0F);
		this.shape4.addBox(0.0F, 0.0F, 0.0F, 28, 8, 1, 0.0F);
		this.shape7_1 = new ModelRenderer(this, 40, -9);
		this.shape7_1.setRotationPoint(30.0F, 6.0F, 13.0F);
		this.shape7_1.addBox(0.0F, 0.0F, 0.0F, 1, 8, 10, 0.0F);
		this.setRotateAngle(shape7_1, 0.0F, 3.141592653589793F, 0.0F);
		this.shape7 = new ModelRenderer(this, 40, -9);
		this.shape7.setRotationPoint(2.0F, 6.0F, 3.0F);
		this.shape7.addBox(0.0F, 0.0F, 0.0F, 1, 8, 10, 0.0F);
		this.shape4_2 = new ModelRenderer(this, 15, 0);
		this.shape4_2.setRotationPoint(2.0F, 15.0F, 2.0F);
		this.shape4_2.addBox(0.0F, 0.0F, 0.0F, 28, 12, 1, 0.0F);
		this.setRotateAngle(shape4_2, 1.5707963267948966F, 0.0F, 0.0F);
	}
	
	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
	{
		this.shape4_1.render(f5);
		this.shape4.render(f5);
		this.shape7_1.render(f5);
		this.shape7.render(f5);
		this.shape4_2.render(f5);
	}
}