package com.zeitheron.visuals.util;

import com.zeitheron.hammercore.utils.inventory.InventoryDummy;

import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;

public class TopStackHelper
{
	public static NonNullList<ItemStack> getTopItems(IInventory inv, int count)
	{
		NonNullList<ItemStack> top = NonNullList.withSize(count, ItemStack.EMPTY);
		NonNullList<ItemStack> all = NonNullList.create();
		InventoryDummy invd = new InventoryDummy(0);
		invd.inventory = all;
		for(int i = 0; i < inv.getSizeInventory(); ++i)
		{
			ItemStack item = inv.getStackInSlot(i).copy();
			if(count(item, invd) == 0)
			{
				item.setCount(count(item, inv));
				all.add(item);
			}
		}
		all.sort((a, b) -> count(b, invd) - count(a, invd));
		for(int i = 0; i < Math.min(all.size(), count); ++i)
			top.set(i, all.get(i));
		return top;
	}
	
	public static int count(ItemStack stack, IInventory stacks)
	{
		int found = 0;
		for(int i = 0; i < stacks.getSizeInventory(); ++i)
			if(stack.isItemEqual(stacks.getStackInSlot(i)))
				found += stacks.getStackInSlot(i).getCount();
		return found;
	}
}