package com.zeitheron.visuals.world;

import com.zeitheron.visuals.api.OrganicLeftovers;
import com.zeitheron.visuals.init.BlocksV;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.math.BlockPos.MutableBlockPos;
import net.minecraft.world.World;

public class WFOrganicLeftovers extends WFBase
{
	{
		setRegistryName("visuals", "orglefts");
	}
	
	@Override
	public void processPillar(World world, MutableBlockPos pos, int x, int z)
	{
		int h = world.getSeaLevel() - 5;
		for(int y = 0; y < h; ++y)
		{
			pos.setY(y);
			
			if(OrganicLeftovers.REPLACEABLE.contains(world.getBlockState(pos).getBlock()))
				world.setBlockState(pos, BlocksV.ORGANIC_LEFTOVERS.getDefaultState());
		}
	}
}