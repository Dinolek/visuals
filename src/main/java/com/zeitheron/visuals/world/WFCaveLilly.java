package com.zeitheron.visuals.world;

import java.util.Random;

import com.zeitheron.hammercore.utils.ChunkUtils;
import com.zeitheron.hammercore.world.gen.IWorldGenFeature;
import com.zeitheron.visuals.api.FallingFix;

import net.minecraft.block.BlockFalling;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.BlockPos.MutableBlockPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import net.minecraftforge.registries.IForgeRegistryEntry;

public class WFCaveLilly extends WFBase
{
	{
		setRegistryName("visuals", "waterlilly");
	}
	
	@Override
	public void processPillar(World world, MutableBlockPos pos, int x, int z)
	{
		int h = world.getSeaLevel() - 5;
		for(int y = 0; y < h; ++y)
		{
			pos.setY(y);
			
			if(world.getBlockState(pos).getBlock() == Blocks.WATERLILY)
				world.setBlockToAir(pos);
		}
	}
}