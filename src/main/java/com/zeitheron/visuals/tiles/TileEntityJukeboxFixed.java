package com.zeitheron.visuals.tiles;

import com.zeitheron.hammercore.net.HCNet;
import com.zeitheron.hammercore.net.internal.PacketSyncAnyTile;
import com.zeitheron.hammercore.tile.ITileDroppable;
import com.zeitheron.hammercore.utils.WorldUtil;

import net.minecraft.block.BlockJukebox;
import net.minecraft.block.BlockJukebox.TileEntityJukebox;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.NetworkRegistry.TargetPoint;

public class TileEntityJukeboxFixed extends TileEntityJukebox implements ITickable, ITileDroppable
{
	public boolean insert;
	public float insertProgress, prevInsertProgress;
	
	public ItemStack dropStack = ItemStack.EMPTY;
	
	@Override
	public void update()
	{
		prevInsertProgress = insertProgress;
		
		if(insert)
			insertProgress = Math.min(1, insertProgress + .05F);
		else
		{
			insertProgress = Math.max(0, insertProgress - .1F);
			if(insertProgress == 0F && !dropStack.isEmpty())
			{
				if(!world.isRemote)
				{
					EntityItem ei = WorldUtil.spawnItemStack(world, pos.getX() + .5, pos.getY() + 1, pos.getZ() + .5, dropStack.copy());
					ei.motionY = .1;
					ei.hoverStart = (float) Math.toRadians(90);
				}
				dropStack = ItemStack.EMPTY;
			}
		}
	}
	
	@Override
	public void setRecord(ItemStack recordStack)
	{
		if(world != null)
		{
			world.setBlockState(pos, world.getBlockState(pos).withProperty(BlockJukebox.HAS_RECORD, !recordStack.isEmpty()), 2);
			validate();
			world.setTileEntity(pos, this);
			insert = !recordStack.isEmpty();
			if(!dropStack.isEmpty())
			{
				if(!world.isRemote)
				{
					EntityItem ei = WorldUtil.spawnItemStack(world, pos.getX() + .5, pos.getY() + 1, pos.getZ() + .5, dropStack.copy());
					ei.motionY = .1;
					ei.hoverStart = (float) Math.toRadians(90);
				}
				dropStack = ItemStack.EMPTY;
			}
		}
		
		super.setRecord(recordStack);
		
		if(world != null && !world.isRemote)
			HCNet.INSTANCE.sendToAllAround(new PacketSyncAnyTile(this), new TargetPoint(world.provider.getDimension(), pos.getX() + .5, pos.getY() + .5, pos.getZ() + .5, 256));
	}
	
	@Override
	public SPacketUpdateTileEntity getUpdatePacket()
	{
		return new SPacketUpdateTileEntity(pos, 0, getUpdateTag());
	}
	
	@Override
	public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt)
	{
		handleUpdateTag(pkt.getNbtCompound());
	}
	
	@Override
	public NBTTagCompound getUpdateTag()
	{
		return serializeNBT();
	}
	
	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound compound)
	{
		NBTTagCompound nbt = super.writeToNBT(compound);
		nbt.setTag("Drop", dropStack.serializeNBT());
		nbt.setBoolean("Insert", insert);
		nbt.setFloat("InsertProg", insertProgress);
		nbt.setFloat("PrevInsertProg", prevInsertProgress);
		return nbt;
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		super.readFromNBT(nbt);
		dropStack = new ItemStack(nbt.getCompoundTag("Drop"));
		insert = nbt.getBoolean("Insert");
		insertProgress = nbt.getFloat("InsertProg");
		prevInsertProgress = nbt.getFloat("PrevInsertProg");
	}
	
	@Override
	public void createDrop(EntityPlayer player, World world, BlockPos pos)
	{
		if(!world.isRemote)
		{
			if(!getRecord().isEmpty())
				WorldUtil.spawnItemStack(world, pos, getRecord());
			if(!dropStack.isEmpty())
				WorldUtil.spawnItemStack(world, pos, dropStack);
		}
	}
}