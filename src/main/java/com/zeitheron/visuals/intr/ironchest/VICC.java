package com.zeitheron.visuals.intr.ironchest;

import cpw.mods.ironchest.common.tileentity.chest.TileEntityCopperChest;
import cpw.mods.ironchest.common.tileentity.chest.TileEntityDiamondChest;
import cpw.mods.ironchest.common.tileentity.chest.TileEntityGoldChest;
import cpw.mods.ironchest.common.tileentity.chest.TileEntityIronChest;
import cpw.mods.ironchest.common.tileentity.chest.TileEntityObsidianChest;
import cpw.mods.ironchest.common.tileentity.chest.TileEntitySilverChest;
import net.minecraftforge.fml.client.registry.ClientRegistry;

public class VICC extends VICS
{
	@Override
	public void init()
	{
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityCopperChest.class, new TESRIronChestModified());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityDiamondChest.class, new TESRIronChestModified());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityGoldChest.class, new TESRIronChestModified());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityIronChest.class, new TESRIronChestModified());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityObsidianChest.class, new TESRIronChestModified());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntitySilverChest.class, new TESRIronChestModified());
	}
}