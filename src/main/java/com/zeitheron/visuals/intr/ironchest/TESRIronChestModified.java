package com.zeitheron.visuals.intr.ironchest;

import java.util.Iterator;
import java.util.Random;

import com.zeitheron.visuals.client.tesr.TESRChestModified;
import com.zeitheron.visuals.proxy.ClientProxy;

import cpw.mods.ironchest.client.renderer.chest.TileEntityIronChestRenderer;
import cpw.mods.ironchest.common.blocks.chest.IronChestType;
import cpw.mods.ironchest.common.tileentity.chest.TileEntityIronChest;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.RenderEntityItem;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;

public class TESRIronChestModified extends TileEntityIronChestRenderer
{
	@Override
	public void render(TileEntityIronChest te, double x, double y, double z, float partialTicks, int destroyStage, float alpha)
	{
		super.render(te, x, y, z, partialTicks, destroyStage, alpha);
		
		float f = te.prevLidAngle + (te.lidAngle - te.prevLidAngle) * partialTicks;
		
		if(f > 0F)
		{
			GlStateManager.pushMatrix();
			GlStateManager.translate(x, y + .999F, z + 1);
			GlStateManager.rotate(180, 1, 0, 0);
			
			TESRChestModified.CHEST_INSIDE.render(null, 0, 0, 0, 0, 0, 0.0625F);
			
			GlStateManager.enableAlpha();
			GlStateManager.enableBlend();
			
			GlStateManager.popMatrix();
			
			NonNullList<ItemStack> list = ClientProxy.renderData.get(te.getPos());
			
			if(te.getType() != IronChestType.CRYSTAL)
			{
				GlStateManager.pushMatrix();
				GlStateManager.translate(x + .5F, y - .6F, z + .5F);
				GlStateManager.enableCull();
				Random random = new Random();
				
				random.setSeed(254L);
				int shift = 0;
				float blockScale = .6F;
				float distMult = 1F;
				float timeD = (float) (360.0D * (double) (System.currentTimeMillis() & 16383L) / 16383.0D) - partialTicks;
				if(list.get(1).isEmpty())
				{
					shift = 8;
					blockScale = 0.85F;
				}
				
				GlStateManager.translate(-.5F, .5F, -.5F);
				TESRChestModified.customitem.setWorld(te.getWorld());
				TESRChestModified.customitem.hoverStart = 0;
				Iterator<ItemStack> var19 = list.iterator();
				
				while(var19.hasNext())
				{
					ItemStack item = var19.next();
					if(shift > TESRChestModified.shifts.length || shift > 8)
						break;
					
					if(item.isEmpty())
						++shift;
					else
					{
						float shiftX = TESRChestModified.shifts[shift][0] * distMult;
						float shiftY = TESRChestModified.shifts[shift][1] * distMult;
						float shiftZ = TESRChestModified.shifts[shift][2] * distMult;
						++shift;
						GlStateManager.pushMatrix();
						GlStateManager.translate(shiftX, shiftY, shiftZ);
						GlStateManager.rotate(timeD, 0.0F, 1.0F, 0.0F);
						GlStateManager.scale(blockScale, blockScale, blockScale);
						TESRChestModified.customitem.setItem(item);
						
						TESRChestModified.getRenderEntityItem().doRender(TESRChestModified.customitem, 0.0D, 0.0D, 0.0D, 0.0F, 0);
						GlStateManager.popMatrix();
					}
				}
				
				GlStateManager.popMatrix();
			}
		}
	}
}