package com.zeitheron.visuals.intr.base;

import com.zeitheron.hammercore.mod.ILoadModule;
import com.zeitheron.hammercore.utils.FinalFieldHelper;
import com.zeitheron.hammercore.utils.ILoadable;
import com.zeitheron.visuals.Visuals;

import net.minecraftforge.event.entity.player.PlayerContainerEvent;

public abstract class VisualsCompat implements ILoadModule, ILoadable
{
	public abstract String getClientProxyClass();
	
	public abstract String getServerProxyClass();
	
	private final IProxy proxy = null;
	
	public void createProxy()
	{
		if(proxy == null)
			try
			{
				IProxy proxy = null;
				if(Visuals.proxy.getClass().getName().endsWith("CommonProxy"))
					proxy = IProxy.class.cast(Class.forName(getServerProxyClass()).getDeclaredConstructor().newInstance());
				else if(Visuals.proxy.getClass().getName().endsWith("ClientProxy"))
					proxy = IProxy.class.cast(Class.forName(getClientProxyClass()).getDeclaredConstructor().newInstance());
				FinalFieldHelper.setFinalField(VisualsCompat.class.getDeclaredField("proxy"), this, proxy);
			} catch(IllegalArgumentException | ReflectiveOperationException e)
			{
				throw new RuntimeException("Proxy class not found!", e);
			}
		else
			throw new RuntimeException("Proxy already created!");
	}
	
	public void containerOpen(PlayerContainerEvent.Open e)
	{
		
	}
	
	public void containerClose(PlayerContainerEvent.Close e)
	{
		
	}
	
	public IProxy getProxy()
	{
		return proxy;
	}
}